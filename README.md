# ERPC（Embedded Remote Procedure Call）嵌入式RPC框架

![image](./doc/ERPC.png)

ERPC（Embedded Remote Procedure Call）是**一个简单的、易用的、高效的远程调用框架**。

它不仅实现了远程调用（RPC），还实现了状态通知（观察者模式），并且集成了EFSM、mult_timer、curl、websocket、MQTT等许多功能，同时还支持数据加密（用户可自定义加密算法）、异常监控和完备的日志管理方法。

使用ERPC可简化模块的设计难度，降低模块之间的耦合度，降低开发人员之间的依赖性，从而实现嵌入式Linux系统下的多进程、多线程应用程序的快速实现、成型与部署。

## 目录
- [一、问题](#1)
- [二、内容](#2)
	- [2.1 框架功能](#2.1)
	- [2.2 外围功能](#2.2)
	- [2.3 扩展知识](#2.3)
- [三、虚拟机](#3)
	- [3.1 虚拟机下载](#3.1)
	- [3.2 虚拟机使用](#3.2)
- [四、如何参与本项目？](#4)

## <a name="1">一、问题</a>

随着科技日新月异的快速发展，电子产品的功能越来越多，业务也越来越复杂。以前靠单打独斗完成电子产品的研发的时代已经慢慢远去，更多的是靠一个团队协作共同努力才能完成。这就为电子产品的设计和研发带来了新的问题：
- 团队的协作，有时是跨部门，甚至是跨地域的，这为沟通带来了很大的成本；
- 团队的开发，必然引入团队的管理，管理的成本也随团队的扩展而增加；
- 软件功能的独立性，导致不同功能采用不同的方法实现，接口也并不统一，开发任何新功能都需要研发人员投入大量精力去学习和熟悉，学习成本较高；
- 由于功能的复杂性，而开发周期一般较短，导致缺乏设计，很多产品的研发投入都是一锤子买卖，很难有一次研发投入，多个产品产出的情况，研发成本较高；
- 由于产品功能复杂，缺乏设计，产线生产成本，后续产品技术服务成本等后即成本不断上升，有甚者公司的研发、生产、技服的成本投入比达到1:2:3；

为了解决如上所有问题，我开发了这套ERPC框架：
- 它统一了应用程序开发方法，将编码拆分为功能实现和接口开发，为应用提供了统一标准化的接口方法，从而降低应用与服务开发者之间的沟通成本；
- 它将开发过程拆分成研发与部署两个阶段，这样更加便于管理优化，这也满足公司部门的划分和职能分工，更利于公司运作；
- 它简化了应用开发难度，将模块、进程、线程等过程全部吸收，应用只需要关注业务数据的处理实现和接口的编写，从而降低出错的概率；
- 它自带的监控程序和日志方法，可实时监控程序异常和任何业务的异常，非常便于问题的定位和解决，而不是以前的规避问题；

ERPC的好处远飞如此，我认为它最核心是解决人员依赖的问题，让研发人员将精力放在真正的功能和接口实现，以及功能和性能的优化上面，这无论是对于研发人员，还是公司都是一个逐渐改善全局的、滚雪球式的快速增长方法。

在ERPC中，我还有一个梦想（不认同勿喷啊）：**希望ERPC能够让Linux系统重新拿回中高端电子产品的市场，实现稳定的、底成本的、可持续发展的方案，从而摆脱安卓的笨重、高成本、受google（美国）限制的状态**！

我也非常期待华为的鸿蒙系统尽快发布，涨中国之气，灭美国之风！

## <a name="2">二、内容（持续更新中）</a>

### <a name="2.1">2.1 框架功能</a>

1. [ERPC特性](./doc/Framework/1.features.md)
2. [快速入门](./doc/Framework/2.QuickStart.md)
3. [使用手册](./doc/Framework/3.UserGuide.md)
	- [框架接口](https://gitee.com/simpost/ERPC/blob/master/doc/Framework/3.UserGuide.md#3)
	- [远程调用接口](https://gitee.com/simpost/ERPC/blob/master/doc/Framework/3.UserGuide.md#4)
	- [观察者模式接口](https://gitee.com/simpost/ERPC/blob/master/doc/Framework/3.UserGuide.md#5)
	- [工具集接口](https://gitee.com/simpost/ERPC/blob/master/doc/Framework/3.UserGuide.md#6)
4. [配置文件](./doc/Framework/4.Profile.md)
5. [日志系统](./doc/Framework/5.Logger.md)
6. [事件驱动型状态机（EFSM）](./doc/Framework/6.EFSM.md)
7. [多元定时器](./doc/Framework/7.mult_timer.md)
8. 关于协议
9. 设计方法

### <a name="2.2">2.2 外围功能</a>

1. [cJSON](./doc/Peripheral/1.cJSON.md)
2. [libuv](http://docs.libuv.org/en/v1.x/index.html#)
3. [libcurl](./doc/Peripheral/3.libcurl.md)
4. [libevent](http://libevent.org/)
5. [gflags](./doc/Peripheral/5.gflags.md)
6. MQTT
7. websocket
8. [zlog](http://hardysimpson.github.io/zlog/)

### <a name="2.3">2.3 扩展知识</a>

1. [json-rpc](./doc/Other/1.json-rpc.md)
2. [深入浅出RPC](./doc/Other/2.rpc-theory.md)

## <a name="3">三、虚拟机</a>

为了便于体验和使用，我使用VirtualBox虚拟机构建了一个CentOS7的系统，内部环境（主要是CMake、GCC编译器4.8.5版本）与编译库的一致，下载下来导入即可直接使用。

### <a name="3.1">3.1 虚拟机下载</a>


### <a name="3.2">3.2 虚拟机使用</a>


## <a name="4">四、如何参与本项目？</a>

由于ERPC仅仅只是一个跨线程、跨进程，甚至可以跨CPU的应用层框架，目前没有做具体的软件功能（比如多媒体、蓝牙），欢迎有能力的个人和公司基于本框架提供实质的功能。

我相信，如果基于这套框架实现的功能够完备的话，那么基于Linux系统的物联网的设备终端开发将会像搭积木一样的简单。

提交PR时请参照以下要求：
- 请确保推荐的资源自己使用过
- 提交PR时请注明推荐理由
- 最好提供可执行的服务进程以便体验

资源列表管理收到PR请求后，会定期（每周）在微博转发本周提交的PR列表，并在微博上面听取使用过这些资源的意见，确认通过后，会加入资源大全。

感谢您的贡献！

### 商务合作

![image](https://note.youdao.com/yws/api/personal/file/8FDE8E7383824879AC3979E023F797BF?method=download&shareKey=7a2363e8a761d41cb75c8f91bf2220ca)

### 赞赏与支持

![image](https://note.youdao.com/yws/api/personal/file/F6C0FED500C04F4AA23E00F25B6B124B?method=download&shareKey=007694b71243062af8b410ce2ba3b3cb)
