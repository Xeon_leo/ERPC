/*
 * MIT License
 * 
 * Copyright (c) 2019 极简美 @ konishi5202@163.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __RPC_LOGGER__H__
#define __RPC_LOGGER_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "zlog.h"

#define log_init(path, cat)     dzlog_init(path, cat)
#define log_reload(path)        zlog_reload(path)
#define log_deinit()            zlog_fini()

#define LOG_F    dzlog_fatal
#define LOG_E    dzlog_error
#define LOG_W    dzlog_warn
#define LOG_N    dzlog_notice
#define LOG_I    dzlog_info
#define LOG_D    dzlog_debug

#ifdef LOG_E
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_ERROR(...)             LOG_E(__VA_ARGS__)
    #else
        #define LOG_ERROR(format, args...) LOG_E(format, ##args)
    #endif
#else
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_ERROR(...)             printf(__VA_ARGS__)
    #else
        #define LOG_ERROR(format, args...) printf("[ERROR]  [%s][%s():%d]:" format, __FILE__, __FUNCTION__, __LINE__, ##args)
    #endif
#endif

#ifdef LOG_W
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_WARN(...)              LOG_W(__VA_ARGS__)
    #else
        #define LOG_WARN(format, args...)  LOG_W(format, ##args)
    #endif
#else
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_WARN(...)              printf(__VA_ARGS__)
    #else
        #define LOG_WARN(format, args...)  printf("[WARNING][%s][%s():%d]:" format, __FILE__, __FUNCTION__, __LINE__, ##args)
    #endif
#endif

#ifdef LOG_N
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_NOTICE(...)              LOG_N(__VA_ARGS__)
    #else
        #define LOG_NOTICE(format, args...)  LOG_N(format, ##args)
    #endif
#else
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_NOTICE(...)              printf(__VA_ARGS__)
    #else
        #define LOG_NOTICE(format, args...)  printf("[NOTICE] [%s][%s():%d]:" format, __FILE__, __FUNCTION__, __LINE__, ##args)
    #endif
#endif

#ifdef LOG_I
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_INFO(...)              LOG_I(__VA_ARGS__)
    #else
        #define LOG_INFO(format, args...)  LOG_I(format, ##args)
    #endif
#else
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_INFO(...)              printf(__VA_ARGS__)
    #else
        #define LOG_INFO(format, args...)  printf("[INFO]   [%s][%s():%d]:" format, __FILE__, __FUNCTION__, __LINE__, ##args)
    #endif
#endif

#ifdef LOG_D
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_DEBUG(...)             LOG_D(__VA_ARGS__)
    #else
        #define LOG_DEBUG(format, args...) LOG_D(format, ##args)
    #endif
#else
    #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
        #define LOG_DEBUG(...)
    #else
        #define LOG_DEBUG(format, args...)
    #endif
#endif

#ifdef __cplusplus
}
#endif

#endif // __RPC_LOGGER_H__

